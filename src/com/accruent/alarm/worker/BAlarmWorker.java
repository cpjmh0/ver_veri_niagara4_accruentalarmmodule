package com.accruent.alarm.worker;

import javax.baja.sys.NotRunningException;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.BWorker;
import javax.baja.util.CoalesceQueue;
import javax.baja.util.Worker;
import javax.baja.nre.annotations.NiagaraType;

@NiagaraType
public class BAlarmWorker
    extends BWorker
{
  

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.worker.BAlarmWorker(2979906276)1.0$ @*/
/* Generated Fri Mar 30 09:05:45 CDT 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAlarmWorker.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


  private CoalesceQueue queue = null;
  private Worker worker = null;
  
  
  public Worker getWorker()
  {
    if (worker == null)
    {
        queue = new CoalesceQueue(1000);
        worker = new Worker(queue);
    }
    return worker;
  }
  
  public void postAsync(Runnable r)
  {
    if(!isRunning() || queue == null)
    {
      throw new NotRunningException();
    }
    queue.enqueue(r);
  }

}
