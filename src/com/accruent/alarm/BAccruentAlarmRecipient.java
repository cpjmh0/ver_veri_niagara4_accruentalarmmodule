package com.accruent.alarm;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.alarm.BAlarmRecipient;
import javax.baja.alarm.BAlarmRecord;
import javax.baja.alarm.ext.BAlarmSourceExt;
import javax.baja.alarm.ext.fault.BOutOfRangeFaultAlgorithm;
import javax.baja.alarm.ext.offnormal.BOutOfRangeAlgorithm;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.Action;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BFacets;
import javax.baja.sys.BRelTime;
import javax.baja.sys.BValue;
import javax.baja.sys.Clock;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;
import javax.baja.xml.XElem;
import javax.baja.xml.XText;
import javax.baja.xml.XWriter;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.accruent.alarm.service.BAccruentDiscoverAlarmService;
import com.accruent.alarm.worker.BAlarmWorker;


@NiagaraType
@NiagaraProperty(
  name = "asyncHandler",
  type = "BAlarmWorker",
  defaultValue = "new BAlarmWorker()"
)
@NiagaraAction(
  name = "sendAlarmToAccruent",
  parameterType = "BAlarmRecord",
  defaultValue = "new BAlarmRecord()",
  returnType = "BBoolean",
  flags = Flags.ASYNC|Flags.HIDDEN
)
@NiagaraAction(
  name = "sendHeartBeatToAccruent",
  returnType = "BBoolean",
  flags = Flags.ASYNC
)
public class BAccruentAlarmRecipient
    extends BAlarmRecipient
{
  

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.BAccruentAlarmRecipient(837547269)1.0$ @*/
/* Generated Fri Mar 30 09:05:44 CDT 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code asyncHandler} property.
   * @see #getAsyncHandler
   * @see #setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BAlarmWorker(), null);
  
  /**
   * Get the {@code asyncHandler} property.
   * @see #asyncHandler
   */
  public BAlarmWorker getAsyncHandler() { return (BAlarmWorker)get(asyncHandler); }
  
  /**
   * Set the {@code asyncHandler} property.
   * @see #asyncHandler
   */
  public void setAsyncHandler(BAlarmWorker v) { set(asyncHandler, v, null); }

////////////////////////////////////////////////////////////////
// Action "sendAlarmToAccruent"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code sendAlarmToAccruent} action.
   * @see #sendAlarmToAccruent(BAlarmRecord parameter)
   */
  public static final Action sendAlarmToAccruent = newAction(Flags.ASYNC | Flags.HIDDEN, new BAlarmRecord(), null);
  
  /**
   * Invoke the {@code sendAlarmToAccruent} action.
   * @see #sendAlarmToAccruent
   */
  public BBoolean sendAlarmToAccruent(BAlarmRecord parameter) { return (BBoolean)invoke(sendAlarmToAccruent, parameter, null); }

////////////////////////////////////////////////////////////////
// Action "sendHeartBeatToAccruent"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code sendHeartBeatToAccruent} action.
   * @see #sendHeartBeatToAccruent()
   */
  public static final Action sendHeartBeatToAccruent = newAction(Flags.ASYNC, null);
  
  /**
   * Invoke the {@code sendHeartBeatToAccruent} action.
   * @see #sendHeartBeatToAccruent
   */
  public BBoolean sendHeartBeatToAccruent() { return (BBoolean)invoke(sendHeartBeatToAccruent, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentAlarmRecipient.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  static Logger logger = Logger.getLogger("BAccruentAlarmRecipient.class");

  @Override
  public void handleAlarm(BAlarmRecord bAlarmRecord)
  {
 //   logger.log(Level.INFO, String.format("**********handleAlarm", bAlarmRecord.getName()));
    Context context = this.getSession().getSessionContext();
    this.post(sendAlarmToAccruent, bAlarmRecord, context);
  }

  public IFuture post(Action action, BValue argument, Context cx)
  {
    Invocation work = new Invocation(this, action, argument, cx);
    getAsyncHandler().getWorker();
    getAsyncHandler().postAsync(work);
    return null;
  }
  
  public BBoolean doSendAlarmToAccruent(BAlarmRecord bAlarmRecord)
  {
    BBoolean bIsAlarmSent = BBoolean.make(true);
    boolean bSendAlarm = true;
    BComponent bAccruentDiscoverAlarmServiceComponent = (BAccruentDiscoverAlarmService)Sys.getService(Sys.loadType(BAccruentDiscoverAlarmService.class));
    BAccruentDiscoverAlarmService bAccruentDiscoverAlarmService = (BAccruentDiscoverAlarmService) bAccruentDiscoverAlarmServiceComponent.asComponent();
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    XWriter xWriter = null;
    XElem xParent = null;
    XElem xHeadend = null;
    XElem xClient = null;
    XElem xAlarm = null;
    BFacets alarmFacets = bAlarmRecord.getAlarmData();
    String[] saAlarmInfo = null;
    String controllerName = null;
    String channelName = null;
    String controllerDescription = null;
    String alarmReason = null;
    String siteName = null;
    String siteDescription = null;
    String sTemp = null;
    String sAlarmDescription = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    XMLGregorianCalendar xmlCal = null;
    Calendar alarmDate = null;
    String sAlarmDate = null;
    try
    {
        /* 4.3 needs to get the alarm facets */
        alarmReason = bAlarmRecord.getAlarmData().get("msgText").toString();
      try
      {
        if (null != bAlarmRecord.getAlarmData() &&
            null != bAlarmRecord.getAlarmData().get("sourceName") &&
            null != bAlarmRecord.getAlarmData().get("alarmValue"))
        {
          sAlarmDescription =
              String.format("%s-%s", bAlarmRecord.getAlarmData().get("sourceName").toString(),
                            bAlarmRecord.getAlarmData().get("alarmValue").toString());
        }
        else if (null != bAlarmRecord.getAlarmData() &&
            null != bAlarmRecord.getAlarmData().get("sourceName"))
        {
          sAlarmDescription =
              String.format("%s", bAlarmRecord.getAlarmData().get("sourceName").toString());
        }
        else if (null != bAlarmRecord.getAlarmData() &&
            null != bAlarmRecord.getAlarmData().get("alarmValue"))
        {
          sAlarmDescription =
              String.format("%s", bAlarmRecord.getAlarmData().get("alarmValue").toString());
        }
        else if (null != bAlarmRecord.getAlarmData())
        {
          StringBuffer sbFacets = new StringBuffer("PROBLEM WITH THIS ALARM FACETS");
          for (String sF : alarmFacets.list())
          {
            sbFacets.append(sF);
          }
          sAlarmDescription = sbFacets.toString();
        }
        else
        {
          sAlarmDescription = "Need to find a good alarm description for this alarm";
        }
      }
      catch (Exception e1)
      {
        sAlarmDescription = "Could Not Figure out an Alarm Description";
      }

       //parse the alarm record first on pipe then on slash
       saAlarmInfo = bAlarmRecord.getSource().toString().split("/");
       controllerDescription = saAlarmInfo[saAlarmInfo.length - 2];
       controllerName = saAlarmInfo[saAlarmInfo.length - 3];
       channelName = saAlarmInfo[saAlarmInfo.length - 2];
       if(saAlarmInfo.length <= 1)
       {  
         saAlarmInfo = bAlarmRecord.getSource().toString().split("/");
         controllerDescription = saAlarmInfo[saAlarmInfo.length - 2];
         controllerName = saAlarmInfo[saAlarmInfo.length - 3];
         channelName = saAlarmInfo[saAlarmInfo.length - 2];
       }
       else if(bAccruentDiscoverAlarmService.getAccruentSupervisorFormat())
       {
    	  //local:|station:|slot:/Drivers/AccruentTest/UVO/Generic/points/AccruentTestPoint/OutOfRangeAlarmExt
    	  //                 0      1       2           3    4       5        6                  7
    	   saAlarmInfo = bAlarmRecord.getSource().toString().split("/");
    	   siteName = siteDescription = saAlarmInfo[saAlarmInfo.length - 5];
    	   controllerName = controllerDescription = saAlarmInfo[saAlarmInfo.length - 4];
    	   channelName = saAlarmInfo[saAlarmInfo.length - 2];
       }
       else
       {
          siteName = Sys.getStation().getStationName();
          siteDescription = Sys.getStation().getStationName();
          controllerDescription = saAlarmInfo[saAlarmInfo.length - 3].trim();
          controllerName = saAlarmInfo[saAlarmInfo.length - 3].trim();
          channelName = saAlarmInfo[saAlarmInfo.length - 2].trim();
          if(saAlarmInfo.length == 5)
          {  
             alarmReason = String.format("%s / %s", saAlarmInfo[4].trim(), alarmReason);
          }  
       }
       logger.log(Level.INFO,String.format("Alarm Reason = %s",alarmReason));
       
       //put in for macada bacnet clients
       sTemp = bAlarmRecord.getAlarmData().get("sourceName").toString();
       if(sTemp.contains("BacnetNetwork"))
       {
         controllerName = sTemp.split("BacnetNetwork")[1];
       }
       sTemp = bAlarmRecord.getAlarmData().get("msgText").toString();
       if(sTemp.contains("%lexicon(driver:"))
       {
         alarmReason = bAlarmRecord.getAlarmData().get("msgText").toString().split(":")[1].split("\\)")[0];
       }
       //end of custom macada bacnet client code
       
       
       if(alarmReason.isEmpty())
       {
         if(bAlarmRecord.getSource().toString().split(":").length == 4)
         {
            if(bAlarmRecord.getSource().toString().split(":")[3].contains("/"))
            {
                alarmReason = bAlarmRecord.getSource().toString().split(":")[3].split("/")[bAlarmRecord.getSource().toString().split(":")[3].split("/").length - 1];
            }
         }
         else
         {  
            bSendAlarm = false;
         }   
       }
       
       //build the parent element site_controller_alarm
       xParent = new XElem("site_controller_alarm");
       xParent.addAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
       
       //removed the following line because nobs-rmstage was not happy with it.
       //xParent.addAttr("xsi:noNameSpaceSchema", "http://wbs.verisae.com/DataNett/xsd/siteControllerAlarmImport.xsd");
       
       //build the headend_element for the alarm
       xHeadend = buildXElem("headend_server", bAccruentDiscoverAlarmService.getHeadendServer(), true);

       //build the client element for the alarm
       xClient = new XElem("client");
       xClient.addContent(buildXElem("client_name", bAccruentDiscoverAlarmService.getClientName(), true));
       
       //build the alarm records
       xAlarm = new XElem("alarm");
       xAlarm.addContent(buildXElem("site_name", siteName != null ? siteName : Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("site_desc", siteDescription != null ? siteDescription : Sys.getStation().getStationName(), true));
       //we need to figure out if this is a mapped controller or not.
       controllerName = figureOutControllerName(bAccruentDiscoverAlarmService.getAccruentSupervisorFormat(), controllerName, channelName);
       xAlarm.addContent(buildXElem("controller_name", controllerName, true));
       xAlarm.addContent(buildXElem("controller_desc", controllerDescription, true));
       xAlarm.addContent(buildXElem("controller_device_type", "", false));
       xAlarm.addContent(buildXElem("serial", bAlarmRecord.getUuid().toString(), true));
//       xAlarm.addContent(buildXElem("reason", alarmReason, true));
       
       if(null != bAlarmRecord.getAlarmData().getFacet("alarmCode"))
       {
         xAlarm.addContent(buildXElem("description", String.format("%s - %s - %s",sAlarmDescription, bAlarmRecord.getAlarmClassDisplayName(null), bAlarmRecord.getAlarmData().getFacet("alarmCode").toString()), true));
         
       }
       else
       {  
          xAlarm.addContent(buildXElem("description", sAlarmDescription, true));
       }   
       logger.log(Level.INFO, String.format("Alarm Class  = %s - %s", bAlarmRecord.getAlarmClass(), bAlarmRecord.getAlarmClassDisplayName(null)));
       //build the alarm date in UTC form
       alarmDate = Calendar.getInstance(TimeZone.getTimeZone(alarmFacets.get("TimeZone").toString().split(" ")[0]));
       if(bAlarmRecord.getSourceState().toString().trim().equals("Normal"))
       {
         xAlarm.addContent(buildXElem("status", "cleared", false));
         alarmDate.setTimeInMillis(bAlarmRecord.getNormalTime().getMillis());
         // if we have a clear then we need to make sure that we put the original text back into the alarm
         // will be either a lowLimit or highLimit 
         // get the clear values if necessary....    
         try
         {
            BOrd bord = BOrd.make(bAlarmRecord.getSource().toString());
            BAlarmSourceExt base = (BAlarmSourceExt) BOrd.make(bord.toString()).get().asComponent();
            try
            {
            BOutOfRangeAlgorithm bora = (BOutOfRangeAlgorithm)base.getOffnormalAlgorithm();
            BOutOfRangeFaultAlgorithm borfa = (BOutOfRangeFaultAlgorithm)base.getFaultAlgorithm();
            if(null != base.getLastOffnormalTime())
            {
              if(null != bAlarmRecord.getAlarmData().getFacet("lowLimit"))
              {  
                 alarmReason = bora.getLowLimitText().encodeToString();
              }
              else
              {
                 alarmReason = bora.getHighLimitText().encodeToString();
              }
            }
            else
            {
              if(null != bAlarmRecord.getAlarmData().getFacet("lowLimit"))
              {
                 alarmReason = borfa.getLowLimitText().encodeToString();
              }
              else
              {
                 alarmReason = borfa.getHighLimitText().encodeToString();
              }
            }
            }
            catch(Exception e)
            {
              alarmReason = base.getToOffnormalText().encodeToString();  //put here on 01272020 in order to get the boolean alarm text so we can clear alarms
            }
            if(alarmReason.equals(""))
            {
            	alarmReason = saAlarmInfo[saAlarmInfo.length - 1];
            }
         }
         catch(Exception e)
         {
            logger.log(Level.SEVERE, String.format("Problem Getting Alarm Reason from Extension %s", e.getMessage()));
         }
         xAlarm.addContent(buildXElem("reason", alarmReason, true));
       }
       else
       {
         xAlarm.addContent(buildXElem("reason", alarmReason, true));
         xAlarm.addContent(buildXElem("status", "occurred", false));
         alarmDate.setTimeInMillis(bAlarmRecord.getTimestamp().getMillis());
       }
       sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
       sAlarmDate = sdf.format(alarmDate.getTime());
       xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(sAlarmDate);
       xAlarm.addContent(buildXElem("tstamp", xmlCal.toString(), false));

       //add the alarm to the client
       xClient.addContent(xAlarm);

       //build the xmlWriter
       bos = new ByteArrayOutputStream();
       xWriter = new XWriter(bos);
       
       //build the output
       xParent.addContent(xHeadend);
       xParent.addContent(xClient);
       xParent.write(xWriter);
       xWriter.flush();
       xWriter.close();
       logger.log(Level.INFO, String.format("Acrruent Alarm = %s",  bos.toString()));
     //  dumpAlarmFacets(bAlarmRecord);
       if(bSendAlarm)
       {  
          bIsAlarmSent = isAlarmSent(bAccruentDiscoverAlarmService.getWebURL(), bAccruentDiscoverAlarmService.getWebUserName(), bAccruentDiscoverAlarmService.getWebUserPassword(), bos.toString());
          logger.log(Level.INFO, String.format("Acrruent Alarm Sent= %s %s",   bAlarmRecord.getSource(), bAlarmRecord.getUuid().toString()));
       }
       else
       {
           logger.log(Level.INFO, String.format("Acrruent Alarm Not Sent= %s %s",   bAlarmRecord.getSource(), bAlarmRecord.getUuid().toString()));
           logger.log(Level.INFO, String.format("Alarm Record Source = %s", bAlarmRecord.getSource().toString()));
       }
    }
    catch(Exception e)
    {
      bIsAlarmSent = BBoolean.make(false);
      logger.log(Level.SEVERE, String.format("Problem Sending Alarm %s %s", e.getMessage(), bAlarmRecord.getSource()));
      e.printStackTrace();
    }
    return bIsAlarmSent;
  }

  private void dumpAlarmFacets(BAlarmRecord bAlarmRecord) throws Exception 
  {
    BFacets alarmFacets = bAlarmRecord.getAlarmData();
    for(String s : alarmFacets.list())
    {
        logger.log(Level.INFO, String.format("**********Facet name = %s, Facet Value %s", s, alarmFacets.get(s)));
    }
  }
  
  private String figureOutControllerName(boolean bAccruentSupervisorFormat, String controllerNamep, String channelNamep) throws Exception
  {
	  String controllerName = null;
	  String controllerKey = String.format("%s|%s",  controllerNamep, channelNamep);
	  controllerKey = controllerKey.replaceAll("\\$", "\\%");
    controllerKey = URLDecoder.decode(controllerKey, "UTF-8");
	  Hashtable<String,String> htControllerNames = readControllerNames(bAccruentSupervisorFormat);
	  if(null != htControllerNames && htControllerNames.containsKey(controllerKey))
	  {
		  controllerName = htControllerNames.get(controllerKey);
	  }
	  else
	  {
		  controllerName = controllerNamep;
	  }
	  return controllerName;
  }
  
  private Hashtable<String,String> readControllerNames(boolean bAccruentSupervisorFormat) throws Exception
  {
	Hashtable<String,String> htControllerNames = null;
    BAccruentFileHandler bAccruentFileHandler = new BAccruentFileHandler(new FilePath("^AccruentData/configuredOrds.txt"), logger);
    BufferedReader br = new BufferedReader(new InputStreamReader(bAccruentFileHandler.getDataFile().getInputStream()));
    String sRec = null;
    String[] saRec = null;
    String controllerName = null;
    String channelName = null;
    String virtualControllerName = null;
    while(br.ready())
    {
       if(null == htControllerNames)
       {
         htControllerNames = new Hashtable<String,String>();
       }
       sRec = br.readLine();
       sRec = sRec.replaceAll("\\$", "\\%");
       sRec = URLDecoder.decode(sRec, "UTF-8");
       if(sRec.contains("|"))
       {
         saRec = sRec.split("\\|");
         sRec = saRec[0];
         if(bAccruentSupervisorFormat)
         {
           controllerName = sRec.split("/")[sRec.split("/").length - 3];
         }
         else
         {
           controllerName = sRec.split("/")[sRec.split("/").length - 2];
         }
         channelName = sRec.split("/")[sRec.split("/").length - 1];
         virtualControllerName = saRec[1];
       }
       htControllerNames.put(String.format("%s|%s",  controllerName, channelName), virtualControllerName);
    }
    return htControllerNames;
  }

  
  public BBoolean doSendHeartBeatToAccruent()
  {
    BBoolean bIsAlarmSent = BBoolean.make(true);
    BComponent bAccruentDiscoverAlarmServiceComponent = (BAccruentDiscoverAlarmService)Sys.getService(Sys.loadType(BAccruentDiscoverAlarmService.class));
    BAccruentDiscoverAlarmService bAccruentDiscoverAlarmService = (BAccruentDiscoverAlarmService) bAccruentDiscoverAlarmServiceComponent.asComponent();
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    XWriter xWriter = null;
    XElem xParent = null;
    XElem xHeadend = null;
    XElem xClient = null;
    XElem xAlarm = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    XMLGregorianCalendar xmlCal = null;
    Calendar alarmDate = null;
    String sAlarmDate = null;
    try
    {
       //build the parent element site_controller_alarm
       xParent = new XElem("site_controller_alarm");
       xParent.addAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
       
       //removed the following line because nobs-rmstage was not happy with it.
       //xParent.addAttr("xsi:noNameSpaceSchema", "http://wbs.verisae.com/DataNett/xsd/siteControllerAlarmImport.xsd");
       
       //build the headend_element for the alarm
       xHeadend = buildXElem("headend_server", bAccruentDiscoverAlarmService.getHeadendServer(), true);

       //build the client element for the alarm
       xClient = new XElem("client");
       xClient.addContent(buildXElem("client_name", bAccruentDiscoverAlarmService.getClientName(), true));
       
       //build the alarm records
       xAlarm = new XElem("alarm");
       xAlarm.addContent(buildXElem("site_name", Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("site_desc", Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("controller_name", "HEARTBEAT", true));
       xAlarm.addContent(buildXElem("controller_desc", "HEARTBEAT", true));
       xAlarm.addContent(buildXElem("controller_device_type", "", false));
       //build the alarm date in UTC form
       alarmDate = Calendar.getInstance();
       sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
       sAlarmDate = sdf.format(alarmDate.getTime());
       xAlarm.addContent(buildXElem("serial", String.valueOf(alarmDate.getTimeInMillis()), true));
       xAlarm.addContent(buildXElem("reason", "JACE Heartbeat", true));
       xAlarm.addContent(buildXElem("description", String.format("%s - %s", "JACE", "HEARTBEAT"), true));
       xAlarm.addContent(buildXElem("status", "occurred", false));
       xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(sAlarmDate);
       xAlarm.addContent(buildXElem("tstamp", xmlCal.toString(), false));
     //  logger.log(Level.INFO, String.format("UTC Alarm Date = %s", xmlCal));
       //add the alarm to the client
       xClient.addContent(xAlarm);

       //build the xmlWriter
       bos = new ByteArrayOutputStream();
       xWriter = new XWriter(bos);
       
       //build the output
       xParent.addContent(xHeadend);
       xParent.addContent(xClient);
       xParent.write(xWriter);
       xWriter.flush();
       xWriter.close();
      // logger.log(Level.INFO, bos.toString());
       logger.log(Level.INFO, String.format("Acrruent Alarm = %s %s %s %s",
                                            bAccruentDiscoverAlarmService.getWebURL(), 
                                            bAccruentDiscoverAlarmService.getWebUserName(), 
                                            bAccruentDiscoverAlarmService.getWebUserPassword(), 
                                            bos.toString()));
       bIsAlarmSent = isAlarmSent(bAccruentDiscoverAlarmService.getWebURL(), bAccruentDiscoverAlarmService.getWebUserName(), bAccruentDiscoverAlarmService.getWebUserPassword(), bos.toString());
    }
    catch(Exception e)
    {
      bIsAlarmSent = BBoolean.make(false);
      logger.log(Level.SEVERE, String.format("Problem Sending Alarm %s", e.getMessage()));
    }
    return bIsAlarmSent;
  }

  
  
  XElem buildXElem(String elementName, String elementData, boolean isCdata) throws Exception
  {
     XElem xElem = new XElem(elementName);
     XText xText = new XText(elementData);
     xText.setCDATA(isCdata);
     xElem.addContent(xText);
     return xElem;
  }

  BBoolean isAlarmSent(String sUrl, String webUserName, String webUserPassword, String xmlAlarm) throws Exception
  {
    BBoolean isAlarmSent = BBoolean.make(true);
    DataInputStream dis = null;

    byte[] bIn = null;
    HttpURLConnection httpUrlConnection = null;
    HttpsURLConnection httpsUrlConnection = null;
    URL url = null;
    
    //Niagara has used a $ for a prefix to a hex character.  If we change that to a % sign we should be urlEncoded...
    xmlAlarm = xmlAlarm.replaceAll("\\$", "\\%");
    xmlAlarm = URLDecoder.decode(xmlAlarm, "UTF-8");
    
    //build the url  Note that we are using the new encoding technique.
    sUrl  = String.format("%s?login=%s&password=%s&loginPage=webservice&xml=%s", sUrl, webUserName, webUserPassword, URLEncoder.encode(xmlAlarm, "UTF-8"));
    url = new URL(sUrl);
    
//    logger.log(Level.INFO, String.format("%s %s", sUrl, xmlAlarm));
    
    //build the connection
    if(sUrl.startsWith("https"))
    {
       httpsUrlConnection = (HttpsURLConnection)url.openConnection();
       httpsUrlConnection.setRequestMethod("POST");
       httpsUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
       httpsUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       httpsUrlConnection.setRequestProperty("enctype", "multipart/form-data");
       httpsUrlConnection.setRequestProperty("Content-Length", "0");
       httpsUrlConnection.setDoInput(true);
       httpsUrlConnection.setDoOutput(true);
       httpsUrlConnection.connect();
       dis = new DataInputStream(httpsUrlConnection.getInputStream());
    }
    else
    {
       httpUrlConnection = (HttpURLConnection)url.openConnection();
       httpUrlConnection.setRequestMethod("POST");
       httpUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
       httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       httpUrlConnection.setRequestProperty("enctype", "multipart/form-data");
       httpUrlConnection.setRequestProperty("Content-Length", "0");
       httpUrlConnection.setDoInput(true);
       httpUrlConnection.connect();
       dis = new DataInputStream(httpUrlConnection.getInputStream());
    }
    logger.log(Level.INFO, String.format("Sending Alarm Now..."));
    Thread.sleep(3000);
    while(dis.available() > 0)
    {
        bIn = new byte[dis.available()];
        dis.read(bIn);
        logger.log(Level.INFO, new String(bIn));
    }
    if(sUrl.startsWith("https"))
       logger.log(Level.INFO, String.format("AlarmSent... Result = %d - %s", httpsUrlConnection.getResponseCode(), httpsUrlConnection.getResponseMessage()));
    else
      logger.log(Level.INFO, String.format("AlarmSent... Result = %d - %s", httpUrlConnection.getResponseCode(), httpUrlConnection.getResponseMessage()));
    dis.close();
    return isAlarmSent;
  }
  @Override
  public void stationStarted()
  {
    //start the heartbeat process
    Clock.schedulePeriodically(this, BRelTime.makeMinutes(60), this.getAction("sendHeartBeatToAccruent"), null);
  }
}
