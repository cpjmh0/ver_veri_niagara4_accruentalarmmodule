package com.accruent.alarm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.alarm.BAlarmClass;
import javax.baja.alarm.BAlarmService;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.Action;
import javax.baja.sys.BAbstractService;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BLink;
import javax.baja.sys.BValue;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Slot;
import javax.baja.sys.Sys;
import javax.baja.sys.Topic;
import javax.baja.sys.Type;

import com.accruent.alarm.BAccruentAlarmRecipient;

@NiagaraType
@NiagaraProperty(
  name = "clientName",
  type = "String",
  defaultValue = "new String()"
)
@NiagaraProperty(
  name = "headendServer",
  type = "String",
  defaultValue = "new String()"
)
@NiagaraProperty(
  name = "webUserName",
  type = "String",
  defaultValue = "new String()"
)
@NiagaraProperty(
  name = "webUserPassword",
  type = "String",
  defaultValue = "new String()"
)
@NiagaraProperty(
  name = "webURL",
  type = "String",
  defaultValue = "new String()"
)
@NiagaraProperty(
		  name = "AccruentSupervisorFormat",
		  type = "BBoolean",
		  defaultValue = "BBoolean.make(false)"
		)
@NiagaraProperty(
  name = "accruentAlarmRecipient",
  type = "BAccruentAlarmRecipient",
  defaultValue = "new BAccruentAlarmRecipient()"
)

@NiagaraAction(
  name = "discoverAlarmClasses",
  returnType = "BValue",
  flags = Flags.SUMMARY
)

@NiagaraAction(
  name = "configureModule",
  returnType = "BValue",
  flags = Flags.SUMMARY
)

public class BAccruentDiscoverAlarmService
    extends BAbstractService
{
  

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.service.BAccruentDiscoverAlarmService(3955784783)1.0$ @*/
/* Generated Wed Jul 07 13:23:54 CDT 2021 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "clientName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code clientName} property.
   * @see #getClientName
   * @see #setClientName
   */
  public static final Property clientName = newProperty(0, new String(), null);
  
  /**
   * Get the {@code clientName} property.
   * @see #clientName
   */
  public String getClientName() { return getString(clientName); }
  
  /**
   * Set the {@code clientName} property.
   * @see #clientName
   */
  public void setClientName(String v) { setString(clientName, v, null); }

////////////////////////////////////////////////////////////////
// Property "headendServer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code headendServer} property.
   * @see #getHeadendServer
   * @see #setHeadendServer
   */
  public static final Property headendServer = newProperty(0, new String(), null);
  
  /**
   * Get the {@code headendServer} property.
   * @see #headendServer
   */
  public String getHeadendServer() { return getString(headendServer); }
  
  /**
   * Set the {@code headendServer} property.
   * @see #headendServer
   */
  public void setHeadendServer(String v) { setString(headendServer, v, null); }

////////////////////////////////////////////////////////////////
// Property "webUserName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code webUserName} property.
   * @see #getWebUserName
   * @see #setWebUserName
   */
  public static final Property webUserName = newProperty(0, new String(), null);
  
  /**
   * Get the {@code webUserName} property.
   * @see #webUserName
   */
  public String getWebUserName() { return getString(webUserName); }
  
  /**
   * Set the {@code webUserName} property.
   * @see #webUserName
   */
  public void setWebUserName(String v) { setString(webUserName, v, null); }

////////////////////////////////////////////////////////////////
// Property "webUserPassword"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code webUserPassword} property.
   * @see #getWebUserPassword
   * @see #setWebUserPassword
   */
  public static final Property webUserPassword = newProperty(0, new String(), null);
  
  /**
   * Get the {@code webUserPassword} property.
   * @see #webUserPassword
   */
  public String getWebUserPassword() { return getString(webUserPassword); }
  
  /**
   * Set the {@code webUserPassword} property.
   * @see #webUserPassword
   */
  public void setWebUserPassword(String v) { setString(webUserPassword, v, null); }

////////////////////////////////////////////////////////////////
// Property "webURL"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code webURL} property.
   * @see #getWebURL
   * @see #setWebURL
   */
  public static final Property webURL = newProperty(0, new String(), null);
  
  /**
   * Get the {@code webURL} property.
   * @see #webURL
   */
  public String getWebURL() { return getString(webURL); }
  
  /**
   * Set the {@code webURL} property.
   * @see #webURL
   */
  public void setWebURL(String v) { setString(webURL, v, null); }

////////////////////////////////////////////////////////////////
// Property "AccruentSupervisorFormat"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code AccruentSupervisorFormat} property.
   * @see #getAccruentSupervisorFormat
   * @see #setAccruentSupervisorFormat
   */
  public static final Property AccruentSupervisorFormat = newProperty(0, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code AccruentSupervisorFormat} property.
   * @see #AccruentSupervisorFormat
   */
  public boolean getAccruentSupervisorFormat() { return getBoolean(AccruentSupervisorFormat); }
  
  /**
   * Set the {@code AccruentSupervisorFormat} property.
   * @see #AccruentSupervisorFormat
   */
  public void setAccruentSupervisorFormat(boolean v) { setBoolean(AccruentSupervisorFormat, v, null); }

////////////////////////////////////////////////////////////////
// Property "accruentAlarmRecipient"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code accruentAlarmRecipient} property.
   * @see #getAccruentAlarmRecipient
   * @see #setAccruentAlarmRecipient
   */
  public static final Property accruentAlarmRecipient = newProperty(0, new BAccruentAlarmRecipient(), null);
  
  /**
   * Get the {@code accruentAlarmRecipient} property.
   * @see #accruentAlarmRecipient
   */
  public BAccruentAlarmRecipient getAccruentAlarmRecipient() { return (BAccruentAlarmRecipient)get(accruentAlarmRecipient); }
  
  /**
   * Set the {@code accruentAlarmRecipient} property.
   * @see #accruentAlarmRecipient
   */
  public void setAccruentAlarmRecipient(BAccruentAlarmRecipient v) { set(accruentAlarmRecipient, v, null); }

////////////////////////////////////////////////////////////////
// Action "discoverAlarmClasses"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code discoverAlarmClasses} action.
   * @see #discoverAlarmClasses()
   */
  public static final Action discoverAlarmClasses = newAction(Flags.SUMMARY, null);
  
  /**
   * Invoke the {@code discoverAlarmClasses} action.
   * @see #discoverAlarmClasses
   */
  public BValue discoverAlarmClasses() { return (BValue)invoke(discoverAlarmClasses, null, null); }

////////////////////////////////////////////////////////////////
// Action "configureModule"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code configureModule} action.
   * @see #configureModule()
   */
  public static final Action configureModule = newAction(Flags.SUMMARY, null);
  
  /**
   * Invoke the {@code configureModule} action.
   * @see #configureModule
   */
  public BValue configureModule() { return (BValue)invoke(configureModule, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentDiscoverAlarmService.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


  static Logger logger = Logger.getLogger("BAccruentDiscoverAlarmService.class");
  static String SEPARATOR = " : ";
  
  public BAccruentDiscoverAlarmService()
  {
   
  }
  
  public BValue doConfigureModule()
  {
    BBoolean isConfigured = BBoolean.make("false");
    BufferedReader br = null;
    BIFile fAlarmConfig = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/alarmConfig.ini"));
    BIFile fAlarmMapFile = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/alarmMapFile.txt"));
    String inRec = null;
    String key = null;
    String value = null;
    if(null != fAlarmConfig)
    {
      try
      {
         br = new BufferedReader(new InputStreamReader(fAlarmConfig.getInputStream()));
         while(br.ready())
         {
           inRec = br.readLine();
           if(inRec.contains(SEPARATOR))
           {
             key = inRec.split(SEPARATOR)[0];
             value = inRec.split(SEPARATOR)[1];
           }
           switch(key)
           {
             case "ALARM_CLIENTNAME":
               setClientName(value);
               break;
             case "ALARM_HEADENDNAME":
               setHeadendServer(value);
               break;
             case "ALARM_URL":
               setWebURL(value);
               break;
             case "ALARM_USERNAME":
               setWebUserName(value);
               break;
             case "ALARM_PASSWORD":
               setWebUserPassword(value);
               break;
             case "ACCRUENT_SUPERVISOR_FORMAT":
               setAccruentSupervisorFormat(value.toUpperCase().equals("TRUE"));
               break;
           }
         }
         isConfigured = BBoolean.make("true");
         setEnabled(Boolean.TRUE);
         Sys.getStation().save();
         this.start();
         br.close();
      }
      catch(Exception e)
      {
        logger.log(Level.SEVERE, String.format("Error Configuring Accruent Alarm Module %s", e.getMessage()));
      }
    }
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setHeadendServer("");
      setWebURL("");
      setWebUserName("");
      setWebUserPassword("");
      setAccruentSupervisorFormat(Boolean.FALSE);
      Sys.getStation().save();
      stop();
    }

    if(null != fAlarmMapFile)
    {
      try
      {
         br = new BufferedReader(new InputStreamReader(fAlarmMapFile.getInputStream()));
         while(br.ready())
         {
           inRec = br.readLine();
         }
         br.close();
      }
      catch(Exception e)
      {
        logger.log(Level.SEVERE, String.format("Error Reading AlamrMapping File %s", e.getMessage()));
      }
    }
    
    return isConfigured;
  }
  

  public BValue doDiscoverAlarmClasses()
  {
    BComponent accruentAlarmRecipientComponent = (BComponent)this.get("accruentAlarmRecipient");
    BAccruentAlarmRecipient accruentAlarmRecipient = (BAccruentAlarmRecipient)accruentAlarmRecipientComponent.asComponent();
    BLink bAlarmLink = null;     
    BComponent bAlarms = null;
    logger.log(Level.INFO, String.format("%s has %d links available", "364 doDiscoverAlarmClasses", accruentAlarmRecipient.getLinks().length));
    //if (accruentAlarmRecipient.getLinks().length == 0)
    {
      for (Slot slot : Sys.getService(BAlarmService.TYPE).getSlotsArray())
      {
        if (slot.isProperty() && slot.asProperty().getType().getTypeName().endsWith("AlarmClass"))
        {
          bAlarms = (BAlarmClass) Sys.getService(BAlarmService.TYPE).get(slot.getName());
          for (Topic topic : bAlarms.getTopicsArray())
          {
            
            if (topic.getName().equals("alarm"))
            {
              /*indirect method*/
              try
              {
                 BOrd bOrd = bAlarms.getOrdInSession();
                 logger.log(Level.INFO,String.format("*******BOrd = %s", bOrd));
                 bAlarmLink = new BLink(bOrd, "alarm", "routeAlarm", true);
                 accruentAlarmRecipient.add(slot.getName(), bAlarmLink);
              }
              catch(Exception e)
              {
                e.printStackTrace();
                logger.log(Level.SEVERE, e.getMessage());
              }
            }
          }
        }
      }
    }
    for (BLink link : accruentAlarmRecipient.getLinks())
    {
      logger.log(Level.INFO, String.format("Link = %s", link.getName()));
    }
    return bAlarms;
  }
  
  @Override
  public void serviceStarted()
  {
    BBoolean isConfigured = (BBoolean)doConfigureModule(); 
    if(isConfigured.getBoolean())
    {  
       logger.log(Level.INFO, String.format("Alarm Service Starting"));
       doDiscoverAlarmClasses();
    }   
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setHeadendServer("");
      setWebURL("");
      setWebUserName("");
      setWebUserPassword("");
      setAccruentSupervisorFormat(Boolean.FALSE);
      Sys.getStation().save();
      stop();
    }
  }
  
  @Override
  public void serviceStopped()
  {
    //deactivate the links activated
    logger.log(Level.INFO,"Removing Links");
    BComponent accruentAlarmRecipientComponent = (BComponent)this.get("accruentAlarmRecipient");
    BAccruentAlarmRecipient accruentAlarmRecipient = (BAccruentAlarmRecipient)accruentAlarmRecipientComponent.asComponent();
    for(BLink bLink : accruentAlarmRecipient.getLinks())
    {
       logger.log(Level.INFO,(String.format("Removing Link %s", bLink.getName())));
       accruentAlarmRecipient.remove(bLink);
    }
  }
  
  
  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }
}
