package com.accruent.alarm;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.sys.BComponent;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.nre.annotations.NiagaraType;

@NiagaraType
public class BAccruentFileHandler
    extends BComponent
{
  

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.telemetry.data.BAccruentFileHandler(2979906276)1.0$ @*/
/* Generated Mon Apr 02 09:14:16 CDT 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentFileHandler.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
 
//FilePath dataOrds = new FilePath("^AccruentData/configuredOrds.txt");
BIFile dataFile = null;
public BAccruentFileHandler()
{
  
}

public BAccruentFileHandler(FilePath filePath, Logger logger)
{
   try
   {
     //check to see if the file exists.  If not then create it.
     if(null == (dataFile = BFileSystem.INSTANCE.findFile(filePath)))
     {
        dataFile = BFileSystem.INSTANCE.makeFile(filePath);
     }
   }
   catch(Exception e)
   {
      logger.log(Level.SEVERE, e.getMessage()); 
   }
}


public BIFile getDataFile()
{
  return dataFile;
}

public void setDataFile(BIFile dataFile)
{
  this.dataFile = dataFile;
}

}
