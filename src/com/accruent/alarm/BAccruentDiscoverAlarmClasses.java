package com.accruent.alarm;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.alarm.BAlarmClass;
import javax.baja.alarm.BAlarmService;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BComponent;
import javax.baja.sys.BLink;
import javax.baja.sys.BValue;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.LinkCheck;
import javax.baja.sys.Property;
import javax.baja.sys.Slot;
import javax.baja.sys.Sys;
import javax.baja.sys.Topic;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;

import com.accruent.alarm.worker.BAlarmWorker;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraType;

@NiagaraType
@NiagaraProperty(
  name = "asyncHandler",
  type = "BAlarmWorker",
  defaultValue = "new BAlarmWorker()"
)
@NiagaraAction(
  name = "discoverAlarmClasses",
  returnType = "BValue",
  flags = Flags.ASYNC
)
public class BAccruentDiscoverAlarmClasses
    extends BComponent
{
  

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.BAccruentDiscoverAlarmClasses(2911822293)1.0$ @*/
/* Generated Fri Mar 30 09:05:45 CDT 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code asyncHandler} property.
   * @see #getAsyncHandler
   * @see #setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BAlarmWorker(), null);
  
  /**
   * Get the {@code asyncHandler} property.
   * @see #asyncHandler
   */
  public BAlarmWorker getAsyncHandler() { return (BAlarmWorker)get(asyncHandler); }
  
  /**
   * Set the {@code asyncHandler} property.
   * @see #asyncHandler
   */
  public void setAsyncHandler(BAlarmWorker v) { set(asyncHandler, v, null); }

////////////////////////////////////////////////////////////////
// Action "discoverAlarmClasses"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code discoverAlarmClasses} action.
   * @see #discoverAlarmClasses()
   */
  public static final Action discoverAlarmClasses = newAction(Flags.ASYNC, null);
  
  /**
   * Invoke the {@code discoverAlarmClasses} action.
   * @see #discoverAlarmClasses
   */
  public BValue discoverAlarmClasses() { return (BValue)invoke(discoverAlarmClasses, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentDiscoverAlarmClasses.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  static Logger logger = Logger.getLogger("BAccruentDiscoverAlarmClasses.class");

  
  public BValue doDiscoverAlarmClasses()
  {
     BLink bAlarmLink = null;     
     BAccruentAlarmRecipient accruentAlarmRecipient = new BAccruentAlarmRecipient();
     BComponent bAlarms = null;
     logger.log(Level.INFO, "doDiscoverAlarmClasses");
     for(Slot slot : Sys.getService(BAlarmService.TYPE).getSlotsArray())
     {
      if(slot.isProperty() && slot.asProperty().getType().getTypeName().equals("AlarmClass"))
      {
        bAlarms = (BAlarmClass)Sys.getService(BAlarmService.TYPE).get(slot.getName());
        for (Topic topic : bAlarms.getTopicsArray())
        {
          if (topic.getName().equals("alarm"))
          {
            //bAlarmLink = new BLink(bAlarms, bAlarms.getTopic("alarm"), accruentAlarmRecipient.getAction("routeAlarm"));
            //accruentAlarmRecipient.add(String.format("Accruent%sLink", slot.getName()), bAlarmLink);
            //bAlarmLink.activate();
            
            //indirect method
            try
            {
               bAlarmLink = new BLink(BOrd.make(String.format("^|slot:/Services/AlarmService/%s", slot.getName())), "alarm", "routeAlarm", true);
               accruentAlarmRecipient.add(slot.getName(), bAlarmLink);
            }
            catch(Exception e)
            {
              logger.log(Level.SEVERE, e.getMessage());
            }
          }
        }
      }
     }
     for (BLink link : accruentAlarmRecipient.getLinks())
     {
       System.out.println(String.format("Link = %s", link.getName()));
     }
     return bAlarms;
  }
  
  
  public IFuture post(Action action, BValue argument, Context cx)
  {
    Invocation work = new Invocation(this, action, argument, cx);
    getAsyncHandler().postAsync(work);
    return null;
  }

}
